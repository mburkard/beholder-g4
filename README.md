# Beholder

Godot 4 addon providing a custom camera node with movement and utilities.

## Default Controls

Movement control has two modes, mouse and keyboard.

### Keyboard Movement

Hold right click and drag mouse to rotate view.

| Action Name           | Description                | Default Key | Default Gamepad   |
|-----------------------|----------------------------|-------------|-------------------|
| beholder_forward      | Move left                  | W           | Left stick up     |
| beholder_left         | Move left                  | A           | Left stick left   |
| beholder_back         | Move back                  | S           | Left stick right  |
| beholder_right        | Move right                 | D           | Left stick down   |
| beholder_rise         | Move up                    | SPACE       | D-pad up          |
| beholder_drop         | Move down                  | Z           | D-pad down        |
| beholder_rotate_up    | Rotate up                  | UP          | Right stick up    |
| beholder_rotate_left  | Rotate left                | LEFT        | Right stick left  |
| beholder_rotate_down  | Rotate down                | DOWN        | Right stick right |
| beholder_rotate_right | Rotate right               | RIGHT       | Right stick down  |
| beholder_boost        | Increase speed while held  | SHIFT       | Press left stick  |
| beholder_slow         | Decrease speed while held  | ALT         | Press right stick |
| beholder_mount        | Toggle mobility            | M           | Bottom action (A) |
| beholder_flashlight   | Toggle flashlight          | F           | Top action (Y)    |
| beholder_emit         | Create projectile instance | E           | Right trigger     |

### Mouse Movement

Hold left click and drag mouse to rotate view.
Hold left click + shift and drag mouse to move camera.

| Action Name         | Default Key | Description                |
|---------------------|-------------|----------------------------|
| beholder_mount      | M           | Toggle mobility            |
| beholder_flashlight | F           | Toggle flashlight          |
| beholder_emit       | E           | Create projectile instance |

## Node Settings

| Variable             | Effect                                             | Default |
|----------------------|----------------------------------------------------|---------|
| Use Mouse Movement   | If enabled, mouse movement control will be used    | false   |
| Speed                | Base movement speed of the camera                  | 4.0     |
| Enable Collisions    | If enabled, camera collides with objects           | false   |
| Boost Scale          | Speed multiplier when boost is enabled             | 1.25    |
| Slow Scale           | Slow multiplier when slow is enabled               | 4.0     |
| Emit Projectile      | PackedScene to instantiate on projectile emit      | null    |
| Emit Force           | Amount of force to apply to emitted projectiles    | 30.0    |
| Max Emit Objects     | Max number of projectiles allowed to exist at once | 100     |
| Emit Cooldown        | Frames between projectile emissions                | 5       |
| Rotation Sensitivity | Rotation speed multiplier                          | 1.0     |

## Additional Notes

- Scene root for `Emit Projectile` must be, or inherit from,
  [RigidBody3D](https://docs.godotengine.org/en/stable/classes/class_rigidbody3d.html#class-rigidbody3d).

- `Emit Projectile` setting must be set for `beholder_emit` to work.

## Support The Developer

<a href="https://www.buymeacoffee.com/mburkard" target="_blank">
  <img src="https://cdn.buymeacoffee.com/buttons/v2/default-blue.png"
	   width="217"
	   height="60"
	   alt="Buy Me A Coffee">
</a>
