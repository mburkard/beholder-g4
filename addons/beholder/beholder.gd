@icon("res://addons/beholder/icons/beholder.svg")
class_name Beholder extends Camera3D

# Mouse or keyboard controls, not both.
## Use mouse, rather than keyboard, movement controls.
@export var use_mouse_movement: bool = false
## Base camera movement speed.
@export var speed: float = 6.0
## Enable camera collisions with physics bodies.
@export var enable_collisions: bool = false
## Speed multiplier when boost is active.
@export var boost_scale: float = 1.25
## Speed divider when slow is active.
@export var slow_scale: float = 4.0
## Scene with projectile to emit, scene root must be, or inherit from, RigidBody3D.
@export var emit_projectile: PackedScene = null
## Linear velocity to apply to emitted projectile instances.
@export var emit_force: float = 30.0
## Maximum number of projectiles that can exist at a time.
## Oldest projectiles will vanish as new ones are created when max is reached.
@export var max_emit_objects: int = 100
## Cooldown between projectile emissions measured in frames.
@export var emit_cooldown: int = 5
## Rotation sensitivity multiplier.
@export var rotation_sensitivity: float = 1.0

# Camera Siblings
var spot_light = SpotLight3D.new()
var character = CharacterBody3D.new()
var collision_shape = CollisionShape3D.new()

# Controls Variables
var _is_right_click_pressed: bool = false
var _is_left_click_pressed: bool = false
var _mount: bool = false
var _emit_cooldown: int = 0

# Action Name Variables
var _action_rotate_left: String = "beholder_rotate_left"
var _action_rotate_right: String = "beholder_rotate_right"
var _action_rotate_up: String = "beholder_rotate_up"
var _action_rotate_down: String = "beholder_rotate_down"
var _action_left: String = "beholder_left"
var _action_right: String = "beholder_right"
var _action_up: String = "beholder_forward"
var _action_down: String = "beholder_back"
var _action_rise: String = "beholder_rise"
var _action_drop: String = "beholder_drop"
var _action_boost: String = "beholder_boost"
var _action_slow: String = "beholder_slow"
var _action_mount: String = "beholder_mount"
var _action_light: String = "beholder_flashlight"
var _action_emit: String = "beholder_emit"
var _emit_objects: Array = []

var _rotation = Vector3.ZERO


func _ready() -> void:
	character.add_child(collision_shape)
	character.add_child(spot_light)
	spot_light.visible = false
	spot_light.spot_range = 30.0
	collision_shape.shape = BoxShape3D.new()
	collision_shape.shape.extents = Vector3(0.05, 0.05, 0.05)
	collision_shape.disabled = not enable_collisions
	character.transform = transform
	character.rotation = rotation
	get_parent().call_deferred("add_child", character)

	if not InputMap.has_action(_action_rotate_left):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_LEFT
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 2
		joypad_event.axis_value = -1
		InputMap.add_action(_action_rotate_left)
		InputMap.action_add_event(_action_rotate_left, key_event)
		InputMap.action_add_event(_action_rotate_left, joypad_event)
	if not InputMap.has_action(_action_rotate_right):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_RIGHT
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 2
		joypad_event.axis_value = 1
		InputMap.add_action(_action_rotate_right)
		InputMap.action_add_event(_action_rotate_right, key_event)
		InputMap.action_add_event(_action_rotate_right, joypad_event)
	if not InputMap.has_action(_action_rotate_up):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_UP
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 3
		joypad_event.axis_value = -1
		InputMap.add_action(_action_rotate_up)
		InputMap.action_add_event(_action_rotate_up, key_event)
		InputMap.action_add_event(_action_rotate_up, joypad_event)
	if not InputMap.has_action(_action_rotate_down):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_DOWN
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 3
		joypad_event.axis_value = 1
		InputMap.add_action(_action_rotate_down)
		InputMap.action_add_event(_action_rotate_down, key_event)
		InputMap.action_add_event(_action_rotate_down, joypad_event)
	if not InputMap.has_action(_action_left):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_A
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 0
		joypad_event.axis_value = -1
		InputMap.add_action(_action_left)
		InputMap.action_add_event(_action_left, key_event)
		InputMap.action_add_event(_action_left, joypad_event)
	if not InputMap.has_action(_action_right):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_D
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 0
		joypad_event.axis_value = 1
		InputMap.add_action(_action_right)
		InputMap.action_add_event(_action_right, key_event)
		InputMap.action_add_event(_action_right, joypad_event)
	if not InputMap.has_action(_action_up):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_W
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 1
		joypad_event.axis_value = -1
		InputMap.add_action(_action_up)
		InputMap.action_add_event(_action_up, key_event)
		InputMap.action_add_event(_action_up, joypad_event)
	if not InputMap.has_action(_action_down):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_S
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 1
		joypad_event.axis_value = 1
		InputMap.add_action(_action_down)
		InputMap.action_add_event(_action_down, key_event)
		InputMap.action_add_event(_action_down, joypad_event)
	if not InputMap.has_action(_action_rise):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_SPACE
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 11
		InputMap.add_action(_action_rise)
		InputMap.action_add_event(_action_rise, key_event)
		InputMap.action_add_event(_action_rise, joypad_event)
	if not InputMap.has_action(_action_drop):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_Z
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 12
		InputMap.add_action(_action_drop)
		InputMap.action_add_event(_action_drop, key_event)
		InputMap.action_add_event(_action_drop, joypad_event)
	if not InputMap.has_action(_action_boost):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_SHIFT
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 7
		InputMap.add_action(_action_boost)
		InputMap.action_add_event(_action_boost, key_event)
		InputMap.action_add_event(_action_boost, joypad_event)
	if not InputMap.has_action(_action_slow):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_ALT
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 8
		InputMap.add_action(_action_slow)
		InputMap.action_add_event(_action_slow, key_event)
		InputMap.action_add_event(_action_slow, joypad_event)
	if not InputMap.has_action(_action_mount):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_M
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 0
		InputMap.add_action(_action_mount)
		InputMap.action_add_event(_action_mount, key_event)
		InputMap.action_add_event(_action_mount, joypad_event)
	if not InputMap.has_action(_action_light):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_F
		var joypad_event = InputEventJoypadButton.new()
		joypad_event.button_index = 3
		InputMap.add_action(_action_light)
		InputMap.action_add_event(_action_light, key_event)
		InputMap.action_add_event(_action_light, joypad_event)
	if not InputMap.has_action(_action_emit):
		var key_event = InputEventKey.new()
		key_event.keycode = KEY_E
		var joypad_event = InputEventJoypadMotion.new()
		joypad_event.axis = 5
		InputMap.add_action(_action_emit)
		InputMap.action_add_event(_action_emit, key_event)
		InputMap.action_add_event(_action_emit, joypad_event)


func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_RIGHT:
			_is_right_click_pressed = event.is_pressed()
		elif event.button_index == MOUSE_BUTTON_LEFT:
			_is_left_click_pressed = event.is_pressed()
		elif event.button_index == MOUSE_BUTTON_WHEEL_UP:
			var _scroll_speed = speed * 10
			var _speed = _scroll_speed * boost_scale if Input.is_key_pressed(KEY_SHIFT) else _scroll_speed
			character.velocity = character.global_transform.basis.z * -_speed
			character.move_and_slide()
			character.velocity = Vector3.ZERO
		elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
			var _scroll_speed = speed * 10
			var _speed = _scroll_speed * boost_scale if Input.is_key_pressed(KEY_SHIFT) else _scroll_speed
			character.velocity = character.global_transform.basis.z * _speed
			character.move_and_slide()
			character.velocity = Vector3.ZERO

	# Mouse controls.
	if use_mouse_movement:
		_handle_mouse_controls(event)
	else:
		_handle_keyboard_control_mouse_rotate(event)


func _process(_delta: float) -> void:
	transform = character.transform
	rotation = character.rotation

		# Toggled controls.
	if Input.is_action_just_pressed(_action_light):
		spot_light.visible = not spot_light.visible
	if Input.is_action_just_pressed(_action_mount):
		_mount = not _mount
		set_physics_process(not _mount)

	# Object Emissions
	if _emit_cooldown > 0:
		_emit_cooldown -= 1
	if emit_projectile and Input.is_action_pressed(_action_emit) and _emit_cooldown == 0:
		_emit_cooldown = emit_cooldown
		var projectile_instance: PhysicsBody3D = emit_projectile.instantiate()
		projectile_instance.global_transform = character.global_transform
		projectile_instance.linear_velocity = -projectile_instance.transform.basis.z * emit_force
		_emit_objects.append(projectile_instance)
		get_tree().get_root().add_child(projectile_instance)
		# Remove oldest emitions if we are at, or over, the limit.
		if len(_emit_objects) >= max_emit_objects:
			var last = _emit_objects.pop_front()
			last.get_parent().remove_child(last)


func _physics_process(delta: float) -> void:
	if not character.is_inside_tree():
		return

	# Rotation
	var _rotation = Vector3.ZERO
	if Input.is_action_pressed(_action_rotate_left):
		_rotation.y += 1
	if Input.is_action_pressed(_action_rotate_right):
		_rotation.y -= 1
	if Input.is_action_pressed(_action_rotate_up):
		_rotation.x += 1
	if Input.is_action_pressed(_action_rotate_down):
		_rotation.x -= 1
	_rotation.x = clamp(_rotation.x, PI / -2.0, PI / 2.0)
	character.set_rotation(character.rotation + (_rotation.normalized() / 30) * rotation_sensitivity)
	if character.rotation.x > 1.5:
		character.rotation.x = 1.5
	if character.rotation.x < -1.5:
		character.rotation.x = -1.5
	_rotation = Vector3.ZERO

	# Handle _is_boost_pressed/_is_slow_pressed modifiers.
	var speed_multiplier = 1.0
	if Input.is_action_pressed(_action_boost):
		speed_multiplier += boost_scale
	if Input.is_action_pressed(_action_slow):
		speed_multiplier /= slow_scale
	var _speed = speed * speed_multiplier * delta * 60

	# Movement
	# Left and right movement.
	if Input.is_action_pressed(_action_left) and Input.is_action_pressed(_action_right):
		pass
	elif Input.is_action_pressed(_action_right):
		character.velocity += character.global_transform.basis.x
	elif Input.is_action_pressed(_action_left):
		character.velocity -= character.global_transform.basis.x
	# Forward and back movement.
	if Input.is_action_pressed(_action_up) and Input.is_action_pressed(_action_down):
		pass
	elif Input.is_action_pressed(_action_up):
		character.velocity -= character.global_transform.basis.z
	elif Input.is_action_pressed(_action_down):
		character.velocity += character.global_transform.basis.z
	# Up and down movement.
	if Input.is_action_pressed(_action_rise):
		character.velocity += character.global_transform.basis.y
	if Input.is_action_pressed(_action_drop):
		character.velocity -= character.global_transform.basis.y
	character.velocity = character.velocity.normalized() * speed * speed_multiplier
	character.move_and_slide()
	character.velocity = Vector3.ZERO


func _handle_keyboard_control_mouse_rotate(event: InputEvent) -> void:
	# Check for mouse motion.
	if event is InputEventMouseMotion:
		if _is_right_click_pressed:
			_drag_rotation(event)
	# Check for mouse button.
	elif event is InputEventMouseButton:
		Input.set_mouse_mode(
			Input.MOUSE_MODE_CAPTURED
			if _is_right_click_pressed else
			Input.MOUSE_MODE_VISIBLE
		)


func _handle_mouse_controls(event: InputEvent) -> void:
	# Check for mouse motion.
	if _is_left_click_pressed and event is InputEventMouseMotion:
		if Input.is_key_pressed(KEY_SHIFT):
			_drag_position(event)
		else:
			_drag_rotation(event)
	Input.set_mouse_mode(
		Input.MOUSE_MODE_CAPTURED
		if _is_left_click_pressed else
		Input.MOUSE_MODE_VISIBLE
	)


func _drag_position(event: InputEventMouseMotion) -> void:
	var _velocity = Vector3.ZERO
	character.velocity.x = character.global_transform.basis.x * -event.relative.x * speed
	character.velocity.y = character.global_transform.basis.y * event.relative.y * speed


func _drag_rotation(event: InputEventMouseMotion) -> void:
	_rotation.y += event.relative.x / -400.0
	_rotation.x = clamp(event.relative.y / -400.0, PI / -2.0, PI / 2.0)
	character.set_rotation(character.rotation + _rotation * rotation_sensitivity)
	if character.rotation.x > 1.5:
		character.rotation.x = 1.5
	if character.rotation.x < -1.5:
		character.rotation.x = -1.5
	_rotation = Vector3.ZERO
