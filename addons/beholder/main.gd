@tool
extends EditorPlugin


func _enter_tree() -> void:
	add_custom_type("Beholder", "Camera3D", preload("beholder.gd"), preload("icons/beholder.svg"))


func _exit_tree() -> void:
	remove_custom_type("Beholder")
